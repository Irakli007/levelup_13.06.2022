import React from "react";
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import styles from './Registration.module.scss';

function Registration() {
    return (
        <div className={styles.registration}>
            <p className={styles.registration_heading}>
                Регистрация
            </p>
            <TextField id="outlined-basic" label="Логин" variant="outlined" />
            <TextField id="outlined-basic" label="Пароль" variant="outlined" />
            <TextField id="outlined-basic" label="Поавторить пароль" variant="outlined" />
            <Button variant="contained">ЗАРЕГИСТРИРОВАТЬСЯ</Button>
            <p className={styles.registration_footer}>
                <Link to="/login"> Если есть аккаунт то авторизуйтесь!</Link>
            </p>
        </div>
    )

}

export default Registration;