import React from "react";
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import styles from './Login.module.scss';

function Login() {
  return (
    <div className={styles.registration}>
      <p className={styles.registration_heading}>
        Вход
      </p>
      <TextField id="outlined-basic" label="Логин" variant="outlined" />
      <TextField id="outlined-basic" label="Пароль" variant="outlined" />
      <Button variant="contained">ВОЙТИ</Button>
      <p className={styles.registration_footer}>
        <Link to="/registration">Нет аккаунта? Зарегестрируйтес.</Link>
      </p>
    </div>
  );
}


export default Login;