import Header from './compoents/Header/Header';
import Todos from './compoents/Todos/Todos';
import Container from '@mui/material/Container';
import Category from './compoents/Category/Category';
import Grid from '@mui/material/Grid';
import Modals from './compoents/Modals/Modals';


function App() {
  return (
    <div>
      <Header />
      <Container maxWidth='xl'>
        <Grid container spacing={2}>
          <Grid item xs={6} md={4}>
            <Todos />
          </Grid>
          <Grid item xs={6} md={8}>
            <Category />
          </Grid>
        </Grid>
      </Container>
      <Modals/>
    </div>
  )
}

export default App;
