import { configureStore } from "@reduxjs/toolkit";
import todosReducer from './todo/slice/index';
import  categoryReducer  from "./Category/slise/index";
import modalsReducer from "./modals/slice/index";

const store = configureStore({
    reducer: {
        todos: todosReducer,
        category: categoryReducer,
        modals: modalsReducer,
    }
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;