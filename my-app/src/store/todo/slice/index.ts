import { createSlice } from '@reduxjs/toolkit'

type Todos = {
    title: string;
    id: number;
};

interface ITodosState {
    todos: Todos[],
};

const initialState: ITodosState = {
    todos: [{ title: 'Title', id: 2 }],
};

export const todosSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        addTodo: (state, { payload }) => ({
            ...state, todos: [...state.todos, payload]
        }),
        removeTodo: (state, { payload }) => ({
            ...state, todos: state.todos.filter((item) => item.id !== payload)
        }),
        updateTodo: (state, {payload}) => ({
            ...state, todos: state.todos.map((item) => item.id === payload.todoId ? {...item, title: payload.title} : item)
        }),
    },
});

export const { addTodo, removeTodo, updateTodo } = todosSlice.actions

export default todosSlice.reducer