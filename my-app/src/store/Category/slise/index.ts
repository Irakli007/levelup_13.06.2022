import { PlaylistAddOutlined } from '@mui/icons-material';
import { createSlice } from '@reduxjs/toolkit'

type CategoryType = {
    category: string;
    id: number;
    todoId: number;
    completed: boolean;
    description?: string;
};

interface CategoryState {
    category: CategoryType[],
};

const initialState: CategoryState = {
    category: [{ category: 'Title', id: 2, todoId: 2, description: '', completed: false }],
};

export const categorySlice = createSlice({
    name: 'category',
    initialState,
    reducers: {
        addCategory: (state, { payload }) => ({
            ...state, category: [...state.category, {...payload, description: '', completed: false }]
        }),
        removeCategory: (state, { payload }) => ({
            ...state, category: state.category.filter((item) => item.id !== payload)
        }),

        editCompletedCategory: (state, { payload }) => ({
            ...state, category: state.category.map((item) => item.id === payload.categoryId ? { ...item, completed: !item.completed } : item)
        }),

        transferCategory: (state,{ payload }) => ({
            ...state, category: state.category.map((item) => item.id === payload.categoryId ? {...item, todoId: payload.todoId} : item)
        }),

    },
});

export const { addCategory, removeCategory, editCompletedCategory, transferCategory } = categorySlice.actions

export default categorySlice.reducer