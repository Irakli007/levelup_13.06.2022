import React, { FC } from "react"; 
import Modal from '@mui/material/Modal';
import Box from "@mui/material/Box";
import Button from '@mui/material/Button';
import styles from '../ModalDeleteCategory/ModalDeleteCategory.module.scss'
import { useAppDispatch } from "../../../store/hook";
import { hideModal, modalsEnums } from "../../../store/modals/slice";
import { removeCategory } from "../../../store/Category/slise";

type ModalDeleteCategoryProps = {
    name: string;
    category?: string;
    categoryId?: number;
};

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #81d4fa',
    boxShadow: 24,
    p: 4,
  };

const ModalDeleteCategory: FC<ModalDeleteCategoryProps> = ({ name, category, categoryId }) => {
    const dispatch = useAppDispatch()

    const handleHideModel = () => dispatch(hideModal({ name: modalsEnums.modalDeleteCategory }));

    const handleRemoveCategory = () => {
        dispatch(removeCategory(categoryId));
        handleHideModel();
    }

   return  <Modal
   open={name === modalsEnums.modalDeleteCategory}
   aria-labelledby="parent-modal-title"
   aria-describedby="parent-modal-description"
 >
   <Box className={styles.wrapper} sx={{ ...style, width: 400 }}>
     <h2 id="parent-modal-title">Подзадача: {category}</h2>
     <p id="parent-modal-description">
       Вы действительно хотите удалить <b>ПОДЗАДАЧУ</b>?
     </p>
     <Box className={styles.buttonGroup}>
     <Button onClick={handleRemoveCategory} variant="contained" color="error">
        ДА
     </Button>
     <Button onClick={handleHideModel} variant="contained" color="success">
        НЕТ
     </Button>
     </Box>
   </Box>
 </Modal>
};

export default ModalDeleteCategory;