import React, { FC } from "react";
import Modal from '@mui/material/Modal';
import Box from "@mui/material/Box";
import Button from '@mui/material/Button';
import styles from '../ModalEditCategory/ModalEditCategory.module.scss';
import { useAppDispatch, useAppSelector } from "../../../store/hook";
import { hideModal, modalsEnums } from "../../../store/modals/slice";
import Todos from "../../Todos/Todos";

type ModalEditCategoryProps = {
    name: string;
    categoryId?: number;
};

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #81d4fa',
    boxShadow: 24,
    p: 2,
};

const ModalEditCategory: FC<ModalEditCategoryProps> = ({ name, categoryId }) => {

    const dispatch = useAppDispatch();
    const { category } = useAppSelector((state) => state.category);
    const categoryFind = category.find((item) => item.id === categoryId);
    const handleHideModel = () => dispatch(hideModal({ name: modalsEnums.modalEditCategory }));

    return <Modal
        open={name === modalsEnums.modalEditCategory}
        aria-labelledby="parent-modal-title"
        aria-describedby="parent-modal-description"
    >
        <Box className={styles.wrapper} sx={{ ...style, width: 400 }}>
            <h2 id="parent-modal-title"> Вы редакитруйте подзадачу: {categoryFind?.category}</h2>
            <Todos name={ name } categoryId={categoryId} />
            <Box className={styles.buttonGroup}>
                <Button onClick={handleHideModel} variant="contained">
                    Закрыть
                </Button>
            </Box>
        </Box>
    </Modal>
};

export default ModalEditCategory;