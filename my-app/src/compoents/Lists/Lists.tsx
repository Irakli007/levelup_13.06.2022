import React, { FC, ReactNode } from "react";
import cn from "classnames";
import styles from './Lists.module.scss';

type ListsProps = {
    children: ReactNode;
    className?: string;
};

export const Lists: FC<ListsProps> = ({ children }) => {
    return <ul className={styles.lists}>
        {children}
    </ul>
};

export const ListItem: FC<ListsProps> = ({ children, className }) => {
    return <li className={cn(styles.list, className )}>
        {children}
    </li>
}