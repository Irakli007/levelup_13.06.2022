import React, { FC } from 'react';
import styles from './button.module.scss';

type ButtonProps = {
    name?: string;
    onClick?: () => void;
}

const Button: FC<ButtonProps> = ({name = "button", onClick}) => {

    return <button className={styles.button} onClick = {onClick}>
        {name}
    </button>
}

export default Button;

