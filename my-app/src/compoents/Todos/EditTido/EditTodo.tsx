import { Box, Button, TextField } from "@mui/material";
import React, { BaseSyntheticEvent, FC, useState } from "react";
import { useAppDispatch } from "../../../store/hook";
import { updateTodo } from "../../../store/todo/slice";
import styles from './EditTodo.module.scss';

type EditTodoProps = {
    title?: string
    todoId: number,
}

const EditTodo: FC<EditTodoProps> = ({ title, todoId }) => {

    const [value, setValue] = useState(title);

    const dispatch = useAppDispatch();

    const handleOnchangeInput = (e: BaseSyntheticEvent) => {
        setValue(e.target.value)
    };

    const handleEditTodo =() => {
        const obj = {
            title: value,
            todoId, 
        }
        dispatch(updateTodo(obj))
    };

    return <Box className={styles.box}>
        <TextField value={value} onChange={handleOnchangeInput} size="small" id="outlined-basic" label="Редактировать задачу" variant="outlined"/>
        <Button variant="contained" size="large" onClick={handleEditTodo}>Редактировать</Button>
    </Box>
}

export default EditTodo;

