import React from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import styles from './AddTodo.module.scss';
import { useAppDispatch } from '../../../store/hook';
import { addTodo } from '../../../store/todo/slice';
import { useForm, Controller } from 'react-hook-form';

type AddTodoform = {
  title: string;
}

let schema = yup.object().shape({
  title: yup.string().required('Поле не должно быть пустым')
})

const AddTodo = () => {
  const { control, handleSubmit, setValue, formState: { errors } } = useForm({
    defaultValues: {
      title: ''
    },
    resolver: yupResolver(schema)
  });



  const dispatch = useAppDispatch();
  const onSumbit = (data: AddTodoform) => {
    dispatch(addTodo({ ...data, id: Date.now() }))
    setValue('title', '')
  }
  return (
    <div className={styles.wrapper}>
      <form onSubmit={handleSubmit(onSumbit)}>
        <Controller name='title' control={control} render={({ field }) => <TextField {...field} error={!!errors?.title}
          helperText={errors?.title?.message}
          size="small"
          id="outlined-basic"
          label="Добавить задачу"
          variant="outlined" />} />
        <Button variant="contained" type='submit' className={styles.button} >Добавить задачу</Button>
      </form>
    </div>
  );
};

export default AddTodo;