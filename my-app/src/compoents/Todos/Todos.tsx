import React, { FC, useState } from 'react';
import ListItemText from '@mui/material/ListItemText';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useAppDispatch, useAppSelector } from '../../store/hook';
import AddTodo from './AddTodos/AddTodo';
import styles from './Todo.module.scss';
import { removeTodo } from '../../store/todo/slice';
import EditTodo from './EditTido/EditTodo'
import { NavLink, useMatch, useNavigate } from 'react-router-dom';
import { Box } from '@mui/material';
import { Lists, ListItem } from '../Lists/Lists';
import cn from 'classnames'
import { hideModal, modalsEnums } from '../../store/modals/slice';
import UndoIcon from '@mui/icons-material/Undo';
import { transferCategory } from '../../store/Category/slise';

type TodosProps = {
    name?: string;
    categoryId?: number;
};

const Todos: FC<TodosProps> = ({ name, categoryId }) => {
    const { todos } = useAppSelector((state) => state.todos);
    const [todoId, setTodoId] = useState<number>(0);
    const [hiding, sethiding] = useState(true);
    const findTodo = todos.find((item) => item.id === todoId);
    const dispatch = useAppDispatch();
    const history = useNavigate();
    const match = useMatch(`/todos/:id`);
    const matchTodoId = Number(match?.params?.id);

    const handleRemoveTodo = (todoId: number) => {
        dispatch(removeTodo(todoId))
    };

    const handleEditTodo = (todoId: number) => {
        setTodoId(todoId);
        sethiding(!hiding);
    };

    const handleTransferCategory = (todoId: number) => {
        dispatch(transferCategory({ categoryId, todoId }));
        dispatch(hideModal({ name: modalsEnums.modalEditCategory }));
        history(`/todos/${todoId}`);
    };

    const isModalEditCategory = name === modalsEnums.modalEditCategory;

    return (
        <>
            {!isModalEditCategory && <AddTodo />}
            <Box className={styles.wrapper}>
                <Lists>
                    {todos.map((item) =>
                        <>
                            <ListItem key={item.id} className={cn(styles.todosLists, { [styles.active]: matchTodoId === item.id })}>
                                <NavLink to={!isModalEditCategory ? `/todos/${item.id}` : '#'} className={styles.link}>
                                    <ListItemText primary={item.title} />
                                </NavLink>
                                <Box className={styles.groupIcon}>
                                    {!isModalEditCategory ?
                                        <>
                                            <EditIcon className={styles.editIkon} onClick={() => handleEditTodo(item.id)} color='primary' />
                                            {hiding && item.id === todoId || <DeleteIcon className={styles.deleteIcon} onClick={() => handleRemoveTodo(item.id)} color='primary' />}
                                        </> : <UndoIcon onClick={() => handleTransferCategory(item.id)} className={styles.revIcon} color='info' />
                                    }
                                </Box>
                            </ListItem>
                            {hiding && item.id === todoId && <EditTodo todoId={todoId} title={findTodo?.title} />}
                        </>
                    )}
                </Lists>
            </Box>
        </>
    );
};

export default Todos;