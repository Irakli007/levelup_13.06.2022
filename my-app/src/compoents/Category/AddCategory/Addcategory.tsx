import { Button, TextField } from "@mui/material";
import React, { FC } from "react";
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from "react-hook-form";
import styles from '../AddCategory/AddCategory.module.scss';
import { addCategory } from "../../../store/Category/slise";
import { useAppDispatch } from "../../../store/hook";

let schema = yup.object().shape({
    category: yup.string().required('Поле не должно быть пустым')
  });

type Categoryform = {
    category: string;
  };

type AddCategoryProps = {
    todoId: number;
  };

const AddCategory: FC<AddCategoryProps> = ({ todoId }) => {

    const { control, handleSubmit, setValue, formState: { errors } } = useForm({
        defaultValues: {
            category: ''
        } as Categoryform,
        resolver: yupResolver(schema)
    });

    const dispatch = useAppDispatch();

    const onSumbit = (data: Categoryform) => {
        dispatch(addCategory({...data, todoId, id: Date.now() }))
        setValue('category', '')
    };

    return <div className={styles.wrapper}>
        <form onSubmit={handleSubmit(onSumbit)}>
            <Controller name='category' control={control} render={({ field }) => <TextField {...field} error={!!errors?.category}
                helperText={errors?.category?.message}
                size="small"
                id="outlined-basic"
                label="Добавить подзадачу"
                variant="outlined" />} />
            <Button className={styles.button} variant="contained" type='submit'>Добавить подзадачу</Button>
        </form>
    </div>
};

export default AddCategory;