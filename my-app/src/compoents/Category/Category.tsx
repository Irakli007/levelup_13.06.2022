import React, { FC } from "react";
import { useMatch } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../store/hook";
import AddCategory from "./AddCategory/Addcategory";
import Checkbox from '@mui/material/Checkbox';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import styles from './category.module.scss';
import Typography from '@mui/material/Typography';
import { Box } from "@mui/system";
import { modalsEnums, showModal } from "../../store/modals/slice";
import { editCompletedCategory } from "../../store/Category/slise";
import { ListItem, Lists } from "../Lists/Lists";

const Category: FC = () => {
    const { category } = useAppSelector((state) => state.category);
    const match = useMatch('todos/:id');

    const dispatch = useAppDispatch();

    const todoId = Number(match?.params?.id);

    const handleRemoveCategory = (categoryId: number, category: string) => {
        dispatch(showModal({ name: modalsEnums.modalDeleteCategory, categoryId, category }))
    };

    const handleChekedCategory = (categoryId: number) => {
        dispatch(editCompletedCategory({ categoryId }))
    };

    return <>
        <AddCategory todoId={todoId} />
        <Lists>
            {category.map((item) => {
                return todoId === item.todoId ?
                    <ListItem key={item.id} className={!item.completed ? styles.list : styles.list_update}>
                        <Box className={styles.category}>
                            <Typography variant="subtitle1" gutterBottom>
                                {item.category}
                            </Typography>
                            <Box className={styles.groupItems}>
                                <EditIcon onClick={() => dispatch(showModal({ name: modalsEnums.modalEditCategory, categoryId: item.id }))} className={styles.editIcon} color='primary' />
                                <Checkbox checked={item.completed} onChange={(e) => handleChekedCategory(item.id)} className={styles.checkbox} />
                                <DeleteIcon className={styles.deleteIcon} onClick={() => handleRemoveCategory(item.id, item.category)} color='primary' />
                            </Box>
                        </Box>
                    </ListItem> : null
            })}
        </Lists>
    </>
}

export default Category;
